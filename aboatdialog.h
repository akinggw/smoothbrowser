﻿#ifndef ABOATDIALOG_H
#define ABOATDIALOG_H

#include <QDialog>

namespace Ui {
class AboatDialog;
}

class AboatDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AboatDialog(QWidget *parent = nullptr);
    ~AboatDialog();

    void setParam(QString title,QString version);

private slots:
    void on_pushButton_clicked();

private:
    Ui::AboatDialog *ui;
};

#endif // ABOATDIALOG_H
