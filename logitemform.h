﻿#ifndef LOGITEMFORM_H
#define LOGITEMFORM_H

#include <QWidget>

namespace Ui {
class LogItemForm;
}

class LogItemForm : public QWidget
{
    Q_OBJECT

public:
    explicit LogItemForm(QString name,QString url,QWidget *parent = nullptr);
    ~LogItemForm();

    void setParam(QString name,QString url);
    void getParam(QString &name,QString &url);

signals:
    /// 删除当前选项
    void signalDeleteCurrentItem(LogItemForm *pform,QString name,QString url);
    /// 用户选中选项
    void signalSelectedCurrentItem(LogItemForm *pform,QString name,QString url);

protected:
    void paintEvent(QPaintEvent *event) override;
    bool eventFilter(QObject *watched, QEvent *event);

private slots:
    void on_pushButton_clicked();

private:
    Ui::LogItemForm *ui;
    QString mName,mUrl;
    bool m_mouseOvered;
};

#endif // LOGITEMFORM_H
