﻿#ifndef MSGMANAGERDIALOG_H
#define MSGMANAGERDIALOG_H

#include <QDialog>

#include "msgrecordform.h"

namespace Ui {
class MsgManagerDialog;
}

class MsgManagerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MsgManagerDialog(QWidget *parent = nullptr);
    ~MsgManagerDialog();

    /// 显示消息管理对话框(0:浏览记录管理；1:书签管理)
    void ShowPage(int type);
    inline bool isMultSelected(void) { return m_isMultSelected; }

protected:
    bool eventFilter(QObject *watched, QEvent *event);

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

    void on_tabWidget_currentChanged(int index);

    void on_tabWidget_tabBarClicked(int index);

private:
    Ui::MsgManagerDialog *ui;

    int m_pageType;
    bool m_isMultSelected;
    MsgRecordForm *m_BrowsingHistoryForm,
                  *m_LabelRecordForm;
};

#endif // MSGMANAGERDIALOG_H
