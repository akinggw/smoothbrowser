﻿#include "showpagesourcedialog.h"
#include "ui_showpagesourcedialog.h"

ShowPageSourceDialog::ShowPageSourceDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ShowPageSourceDialog)
{
    ui->setupUi(this);

    Qt::WindowFlags flags=Qt::Dialog;
    flags |=Qt::WindowCloseButtonHint;
    flags |=Qt::WindowMinimizeButtonHint;
    flags |=Qt::WindowMaximizeButtonHint;
    setWindowFlags(flags);

    m_sciLexerList["js"] = new QsciLexerJavaScript(this);
    m_sciLexerList["html"] = new QsciLexerHTML(this);

    m_editScintilla = new QsciScintilla(this);

    // 设置括号等自动补全
    m_editScintilla->setAutoIndent(true);
    //设置自动补全
    m_editScintilla->setCaretLineVisible(true);
    m_editScintilla->setMarginWidth(0, "000");
    m_editScintilla->setMarginLineNumbers(0, true);
    m_editScintilla->setBraceMatching(QsciScintilla::SloppyBraceMatch);
    m_editScintilla->setTabWidth(4);
    m_editScintilla->setFolding(QsciScintilla::BoxedTreeFoldStyle);//折叠样式
    m_editScintilla->setFoldMarginColors(Qt::gray,Qt::lightGray);//折叠栏颜色
    //行号提示
    m_editScintilla->SendScintilla(QsciScintilla::SCI_SETCODEPAGE,QsciScintilla::SC_CP_UTF8);
    //选中文本背景色
    m_editScintilla->setSelectionBackgroundColor(Qt::red);

    QHash<QString,QsciLexer*>::iterator itersci = m_sciLexerList.find("html");
    if(itersci != m_sciLexerList.end())
    {
        m_editScintilla->setLexer(itersci.value());

        //设置自动补全的字符串和补全方式
        QsciAPIs *apis = new QsciAPIs(itersci.value());
        //if(!apis->load(QString(":/resources/apis.txt")))
        //    QLOG_WARN()<<"apis open failed.";

        apis->prepare();
    }

    m_editScintilla->setAutoCompletionSource(QsciScintilla::AcsAll);   //自动补全所以地方出现的
    m_editScintilla->setAutoCompletionCaseSensitivity(true);   //设置自动补全大小写敏感
    m_editScintilla->setAutoCompletionThreshold(1);    //输入一个字符就会出现自动补全的提示

    ui->verticalLayout->addWidget(m_editScintilla);
}

ShowPageSourceDialog::~ShowPageSourceDialog()
{
    delete ui;
    delete m_editScintilla;

    QHash<QString,QsciLexer*>::iterator iter = m_sciLexerList.begin();
    for(;iter != m_sciLexerList.end();++iter)
    {
        delete iter.value();
    }
}

void ShowPageSourceDialog::showcontent(QString data)
{
    if(data.isEmpty())
        return;

    m_editScintilla->setText(data);
}
