﻿#ifndef NAVIGATIONFORM_H
#define NAVIGATIONFORM_H

#include <QWidget>
#include <QUrl>
#include <QIcon>

#include "cwebengineview.h"

namespace Ui {
class NavigationForm;
}

class NavigationForm : public QWidget
{
    Q_OBJECT

public:
    explicit NavigationForm(QWidget *parent = nullptr);
    ~NavigationForm();

    /// 设置标签标题
    void setTitle(QString title,QUrl url);
    /// 设置标签是否被选中
    void setSelected(bool isselected);
    /// 得到当前url
    inline QUrl getUrl(void) { return m_currentOperUrl; }
    /// 得到当前标题
    inline QString getTitle(void) { return m_currentTitle; }
    /// 设置当前url
    inline void setCurrentUrl(QUrl url) { m_currentUrl = url; }
    /// 设置icon
    void setIcon(const QIcon &icon);
    /// 显示页面源码
    void showPageSource(void);

    /// 显示初始页面
    void showDefaultPage(void);
    /// 显示指定的页面
    void showPage(QUrl url);
    inline CWebEngineView* getWebEngineView(void) { return m_WebEngineView; }
    inline void back(void) { m_WebEngineView->back(); }
    inline void forward(void) { m_WebEngineView->forward(); }
    inline void reload(void) { m_WebEngineView->reload(); }

signals:
    void getHTML(QString sHTML);
    /// 删除自身
    void signaldeleteself(NavigationForm *form);
    /// 打开一个新的标签页
    void signalopennewlab(NavigationForm* tabIndex,const QUrl &url);
    /// 拷贝连接到粘贴板
    void signalCopyClipboard(void);
    /// 下载连接
    void signalDownloadLink(void);
    /// 从一个新的窗口打开
    void signalOpenLinkInNewWindow(void);
    /// 从一个新的标签页打开
    void signalOpenLinkInNewTab(void);

protected:
    void paintEvent(QPaintEvent *event) override;
    bool eventFilter(QObject *watched, QEvent *event);

private slots:
    void on_pushButton_clicked(void);
    /// 打开一个新的标签页
    void onprocesssignalopennewlab(const QUrl &url);
    void mouseClicked(void);

    /// 保存网页
    void OnMenuTriggereSavaHtml(bool checked);
    /// 显示页面源码
    void OnMenuTriggerShowPageHtml(bool checked);
    /// 拷贝连接到剪切板
    void OnMenuTriggerCopyClipboard(bool checked);
    /// 下载连接
    void OnMenuTriggerDownloadLink(bool checked);
    /// 从一个新的窗口打开
    void OnMenuTriggerOpenLinkInNewWindow(bool checked);
    /// 从一个新的标签页打开
    void OnMenuTriggerOpenLinkInNewTab(bool checked);

private:
    Ui::NavigationForm *ui;
    bool m_isSelected;
    QIcon m_currentUsingIcon;
    QUrl m_currentOperUrl,m_currentUrl;
    CWebEngineView *m_WebEngineView;
    QString m_currentTitle;
};

#endif // NAVIGATIONFORM_H
