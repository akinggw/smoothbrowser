﻿#include "navigationform.h"
#include "ui_navigationform.h"

#include <QPainter>
#include <QAction>
#include <QFileDialog>

NavigationForm::NavigationForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NavigationForm),
    m_isSelected(false)
{
    ui->setupUi(this);

    m_currentTitle = QString::fromLocal8Bit("新标签页");

    //this->setFocusPolicy(Qt::StrongFocus);
    //this->installEventFilter(this);

    m_WebEngineView = new CWebEngineView();
    this->setFocusProxy(m_WebEngineView);
    this->focusProxy()->installEventFilter(parent);

    connect(m_WebEngineView->page(),
            SIGNAL(titleChanged(const QString &)),
            parent,
            SLOT(onWebEngineViewtitleChanged(const QString &)));
    connect(m_WebEngineView->page(),
            SIGNAL(urlChanged(const QUrl &)),
            parent,
            SLOT(onWebEngineViewurlChanged(const QUrl &)));

    connect(m_WebEngineView,
            SIGNAL(signalopennewlab(const QUrl &)),
            this,
            SLOT(onprocesssignalopennewlab(const QUrl &)));

    connect(m_WebEngineView->pageAction(QWebEnginePage::SavePage),
            SIGNAL(triggered(bool)),
            this,
            SLOT(OnMenuTriggereSavaHtml(bool)));
    connect(m_WebEngineView->pageAction(QWebEnginePage::ViewSource),
            SIGNAL(triggered(bool)),
            this,
            SLOT(OnMenuTriggerShowPageHtml(bool)));
    connect(m_WebEngineView->pageAction(QWebEnginePage::CopyLinkToClipboard),
            SIGNAL(triggered(bool)),
            this,
            SLOT(OnMenuTriggerCopyClipboard(bool)));
    connect(m_WebEngineView->pageAction(QWebEnginePage::DownloadLinkToDisk),
            SIGNAL(triggered(bool)),
            this,
            SLOT(OnMenuTriggerDownloadLink(bool)));
    connect(m_WebEngineView->pageAction(QWebEnginePage::OpenLinkInNewWindow),
            SIGNAL(triggered(bool)),
            this,
            SLOT(OnMenuTriggerOpenLinkInNewWindow(bool)));
    connect(m_WebEngineView->pageAction(QWebEnginePage::OpenLinkInNewTab),
            SIGNAL(triggered(bool)),
            this,
            SLOT(OnMenuTriggerOpenLinkInNewTab(bool)));
}

NavigationForm::~NavigationForm()
{
    delete ui;
    delete m_WebEngineView;
}

void NavigationForm::mouseClicked(void)
{

}

/**
 * @brief NavigationForm::showDefaultPage 显示初始页面
 */
void NavigationForm::showDefaultPage(void)
{
    m_WebEngineView->load(QUrl("file:///"+
                               QCoreApplication::applicationDirPath()+
                               "/data/default.html"));
    m_WebEngineView->show();
}

/**
 * @brief NavigationForm::showPage 显示指定的页面
 * @param url
 */
void NavigationForm::showPage(QUrl url)
{
    if(m_currentUrl.toString() == url.toString())
        return;

    m_currentUrl = url;

    m_WebEngineView->load(m_currentUrl);
    m_WebEngineView->show();
}

/**
 * @brief NavigationForm::setTitle 设置标签标题
 * @param title
 */
void NavigationForm::setTitle(QString title,QUrl url)
{
    if(!title.isEmpty() &&
        m_currentTitle != title)
    {
        m_currentTitle = title;

        ui->label_2->setText(m_currentTitle);
        ui->label_2->setToolTip(m_currentTitle);
    }

    if(!url.isEmpty())
        m_currentOperUrl = url;
}

/**
 * @brief NavigationForm::OnMenuTriggerCopyClipboard 拷贝连接到剪切板
 * @param checked
 */
void NavigationForm::OnMenuTriggerCopyClipboard(bool checked)
{
    emit signalCopyClipboard();
}

/**
 * @brief NavigationForm::showPageSource 显示页面源码
 */
void NavigationForm::showPageSource(void)
{
    if(m_WebEngineView &&
       m_WebEngineView->page())
    {
        m_WebEngineView->page()->toHtml([this](const QString& result) mutable {emit getHTML(result);});
    }
}

/**
 * @brief NavigationForm::OnMenuTriggerShowPageHtml 显示页面源码
 * @param checked
 */
void NavigationForm::OnMenuTriggerShowPageHtml(bool checked)
{
    showPageSource();
}

/**
 * @brief NavigationForm::OnMenuTriggereSavaHtml 保存网页
 * @param action
 */
void NavigationForm::OnMenuTriggereSavaHtml(bool checked)
{
    if(m_WebEngineView &&
       m_WebEngineView->page())
    {
        QString mCurOpertorFile = QFileDialog::getExistingDirectory(this,
                                                                    QString::fromLocal8Bit("保存目录"),
                                                                    "./");

        if(!mCurOpertorFile.isEmpty())
        {
            mCurOpertorFile += ("/"+m_WebEngineView->page()->title());
            m_WebEngineView->page()->save(mCurOpertorFile,
                                          QWebEngineDownloadItem::CompleteHtmlSaveFormat);
        }
    }
}

void NavigationForm::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    QPen pPen;
    pPen.setWidth(1);

    if(m_isSelected) pPen.setColor(QColor("blue"));
    else pPen.setColor(QColor("gray"));

    painter.setPen(pPen);

    painter.drawRect(0,1,this->width()-3,this->height()-2);
}

void NavigationForm::on_pushButton_clicked()
{
    emit signaldeleteself(this);
}

/**
 * @brief NavigationForm::onprocesssignalopennewlab 打开一个新的标签页
 * @param url
 */
void NavigationForm::onprocesssignalopennewlab(const QUrl &url)
{
    emit signalopennewlab(this,url);
}

/**
 * @brief NavigationForm::setIcon 设置icon
 * @param icon
 */
void NavigationForm::setIcon(const QIcon &icon)
{
    if(icon.isNull())
        return;

    m_currentUsingIcon = icon;
    ui->label->setPixmap(icon.pixmap(QSize(24,24)));
}

/**
 * @brief NavigationForm::setSelected 设置标签是否被选中
 * @param isselected
 */
void NavigationForm::setSelected(bool isselected)
{
    m_isSelected = isselected;

    if(m_currentUsingIcon.isNull())
    {
        if(m_isSelected)
        {
            ui->label->setPixmap(QPixmap(":/resources/ancient-gate-fill.png"));
            //ui->label_2->setStyleSheet("QLabel{color:black;font-weight:normal;}");
        }
        else
        {
            ui->label->setPixmap(QPixmap(":/resources/ancient-gate-line.png"));
            //ui->label_2->setStyleSheet("QLabel{color:grey;font-weight:normal;}");
        }
    }

    this->update();
}

/**
 * @brief NavigationForm::OnMenuTriggerOpenLinkInNewWindow 从一个新的标签页打开
 * @param checked
 */
void NavigationForm::OnMenuTriggerOpenLinkInNewWindow(bool checked)
{
    emit signalOpenLinkInNewWindow();
}

/**
 * @brief NavigationForm::OnMenuTriggerOpenLinkInNewTab 从一个新的标签页打开
 * @param checked
 */
void NavigationForm::OnMenuTriggerOpenLinkInNewTab(bool checked)
{
    emit signalOpenLinkInNewTab();
}

/**
 * @brief NavigationForm::OnMenuTriggerDownloadLink 下载连接
 * @param checked
 */
void NavigationForm::OnMenuTriggerDownloadLink(bool checked)
{
    emit signalDownloadLink();
}

bool NavigationForm::eventFilter(QObject *watched, QEvent *event)
{
    qDebug()<<event->type();
    if(event->type() == QEvent::MouseButtonPress)
    {
        qDebug()<<"QEvent::MouseButtonPress";
    }

    return QWidget::eventFilter(watched,event);
}
