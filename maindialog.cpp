﻿#include "maindialog.h"
#include "ui_maindialog.h"

#include <QMessageBox>
#include <QFile>
#include <qDebug>
#include <QCloseEvent>
#include <QToolButton>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QScrollBar>
#include <QEventLoop>
#include <QProcess>
#include <QClipboard>
#include <QFileDialog>

#include "common/common.h"
#include "widgets/libcmessagebox.h"

#define IDD_LABEL_WIDTH 180

initialiseSingleton(MainDialog);

MainDialog::MainDialog(QWidget *parent,QUrl url)
    : FramelessWindow(parent)
    , ui(new Ui::MainDialog),
      m_isDialogMax(true),
      m_currentNavigationForm(NULL),
      m_LastLogsForm(NULL),
      m_currentWebEnginePage(NULL)
{
    ui->setupUi(this);

    this->setWindowTitle(IDD_MAINDLG_TITLE+IDD_MAINDLG_VERSION);

    init_lib_resources();

    //this->setMouseTracking(true);
    this->installEventFilter(this);
    this->setFocusPolicy(Qt::StrongFocus);

    this->setResize(true);
    this->setShadeEnable(true);
    this->setEnabledDrogging(true);
    this->setDrawTitleBar(false);
    this->setDoubleClickMaxsize(true);
    this->setShadeRounded(false);

    ui->scrollArea->horizontalScrollBar()->setStyleSheet("QScrollBar {height:0px;}");
    ui->scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->scrollArea->verticalScrollBar()->setEnabled(false);
    ui->scrollArea->horizontalScrollBar()->setEnabled(true);

    ui->scrollArea->installEventFilter(this);

    QFile file(":/resources/all.qss");
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qApp->setStyleSheet(file.readAll());
        file.close();
    }

    m_ShowPageSourceDialog = new ShowPageSourceDialog(this);
    m_MsgManagerDialog = new MsgManagerDialog(this);
    m_AboatDialog = new AboatDialog(this);
    m_ToolBarForm = new ToolBarForm(this);
    ui->horizontalLayout_10->addWidget(m_ToolBarForm);

    LoadAllLabels();
    reloadAllLabelControls(true);

    m_addNewLabelBtn = new QPushButton(this);
    m_addNewLabelBtn->setIcon(QIcon(":/resources/add-fill.png"));
    m_addNewLabelBtn->setStyleSheet("QPushButton{border-radius: 5px;}");
    m_addNewLabelBtn->setMaximumSize(QSize(22,22));

    addNewLabel(QString::fromLocal8Bit("新标签页"),
                true,
                -1,
                url);

    connect(m_addNewLabelBtn,
            SIGNAL(pressed()),
            this,
            SLOT(onprocessaddNewLabelBtn()));

    connect(m_ToolBarForm,
            SIGNAL(signaltoLastPage()),
            this,
            SLOT(onprocesssignaltoLastPage()));
    connect(m_ToolBarForm,
            SIGNAL(signaltoNextPage()),
            this,
            SLOT(onprocesssignaltoNextPage()));
    connect(m_ToolBarForm,
            SIGNAL(signalreloadPage()),
            this,
            SLOT(onprocesssignalreloadPage()));
    connect(m_ToolBarForm,
            SIGNAL(signalOpenUrl(QUrl)),
            this,
            SLOT(onprocesssignalOpenUrl(QUrl)));
    connect(m_ToolBarForm,
            SIGNAL(signalSaveLabel(bool)),
            this,
            SLOT(onprocesssignalSaveLabel(bool)));
    connect(m_ToolBarForm,
            SIGNAL(signalSystemSetMenu(void)),
            this,
            SLOT(onprocessSystemSetMenu(void)));

    m_LastLogsForm = new LastLogsForm(this);
    m_LastLogsForm->installEventFilter(this);
    m_LastLogsForm->setWindowFlags(Qt::WindowStaysOnTopHint);
    m_LastLogsForm->hide();

    connect(m_LastLogsForm,
            SIGNAL(signalDeleteCurrentItem(QString,QString)),
            this,
            SLOT(onprocessDeleteCurrentItem(QString,QString)));
    connect(m_LastLogsForm,
            SIGNAL(signalSelectedCurrentItem(QString,QString)),
            this,
            SLOT(onprocessSelectedCurrentItem(QString,QString)));

    m_Menu.addAction(QIcon(":/resources/file-3-line.png"),
                     QString::fromLocal8Bit("打开新的标签页"));
    m_Menu.addAction(QIcon(":/resources/tablet-line.png"),
                     QString::fromLocal8Bit("打开新的窗口"));
    m_Menu.addSeparator();
    m_Menu.addAction(QIcon(":/resources/database-2-line.png"),
                     QString::fromLocal8Bit("历史记录"));
    m_Menu.addAction(QIcon(":/resources/contacts-book-line.png"),
                     QString::fromLocal8Bit("标签管理"));
    m_Menu.addSeparator();
    m_Menu.addAction(QIcon(":/resources/hammer-line.png"),
                     QString::fromLocal8Bit("源代码"));
    m_Menu.addAction(QIcon(":/resources/bug-fill.png"),
                     QString::fromLocal8Bit("调试"));
    m_Menu.addSeparator();
    m_Menu.addAction(QIcon(":/resources/printer-fill.png"),
                     QString::fromLocal8Bit("打印"));
    m_Menu.addAction(QIcon(":/resources/question-mark.png"),
                     QString::fromLocal8Bit("关于"));
    m_Menu.addSeparator();
    m_Menu.addAction(QIcon(":/resources/login-box-line.png"),
                     QString::fromLocal8Bit("退出"));

    QObject::connect(&m_Menu,
                     SIGNAL(triggered(QAction*)),
                     this,
                     SLOT(OnSetMenuTriggered(QAction*)));

    connect(&m_timerShowTip,
            SIGNAL(timeout()),
            this,
            SLOT(timerShowTip()));
    m_timerShowTip.setSingleShot(true);

    m_LoadStateForm = new LoadStateForm(this);
    m_LoadStateForm->hide();

    this->showMaximized();
}

MainDialog::~MainDialog()
{
    delete ui;

    cleanup_lib_resources();

    delete m_ShowPageSourceDialog;
    delete m_ToolBarForm;
    delete m_MsgManagerDialog;
    delete m_LoadStateForm;
    delete m_AboatDialog;
    delete m_LastLogsForm;
    delete m_addNewLabelBtn;
}

/**
 * @brief MainDialog::SshowFullScreen 是否全屏显示
 * @param isShow
 */
void MainDialog::SshowFullScreen(bool isShow)
{
    boxlayoutVisible(ui->horizontalLayout_3,!isShow);
    boxlayoutVisible(ui->horizontalLayout_4,!isShow);
    boxlayoutVisible(ui->horizontalLayout_10,!isShow);
    boxlayoutVisible(ui->horizontalLayout_2,!isShow);

    if(isShow)
    {
        //ui->horizontalLayout_2->setContentsMargins(0,0,0,0);
        this->showFullScreen();
        //this->update();
    }
    else
    {
        //ui->horizontalLayout_2->setContentsMargins(0,0,2,2);
        this->showMaximized();
        this->update();
    }
}

/**
 * @brief MainDialog::showTip 显示提示
 * @param tip
 * @param time
 */
void MainDialog::showTip(QString tip,int time)
{
    QString tmpString = tip;

    QFontMetrics fontMetrics(m_LoadStateForm->font());
    int nFontWidth = fontMetrics.width(tmpString);
    int nFontMaxWidth = this->size().width()/2;

    if(nFontWidth > nFontMaxWidth)
    {
        tmpString = fontMetrics.elidedText(tmpString,Qt::ElideRight,nFontMaxWidth);
        nFontWidth = nFontMaxWidth;
    }

    m_LoadStateForm->setTitle(tmpString);
    m_LoadStateForm->activateWindow();

    QSize tmpStateFormSize = m_LoadStateForm->size();
    m_LoadStateForm->setGeometry(QRect(QPoint(2,this->size().height()-tmpStateFormSize.height()),
                                       QSize(nFontWidth,tmpStateFormSize.height())));

    m_LoadStateForm->raise();
    m_LoadStateForm->show();

    if(time > 0)
    {
        m_timerShowTip.start(time);
    }
}

void MainDialog::timerShowTip(void)
{
    if(m_LoadStateForm->isVisible())
        m_LoadStateForm->hide();
}

void MainDialog::changeEvent(QEvent *event)
{
    if(event->type()!=QEvent::WindowStateChange) return;

    if(this->windowState() == Qt::WindowMaximized)
    {
        ui->pushButton_3->setIcon(QIcon(":/resources/min.png"));
    }
    else
    {
        ui->pushButton_3->setIcon(QIcon(":/resources/max.png"));
    }
}

void MainDialog::closeEvent(QCloseEvent *event)
{
    /*QMessageBox box(QMessageBox::Warning,QString::fromLocal8Bit("退出提示"),
                    QString::fromLocal8Bit("您确定要退出")+
                    IDD_MAINDLG_TITLE+
                    IDD_MAINDLG_VERSION+
                    QString::fromLocal8Bit("?"));
    //box.setParent(this);
    box.setStandardButtons (QMessageBox::Ok|QMessageBox::Cancel);
    box.setButtonText (QMessageBox::Ok,QString::fromLocal8Bit("确 定"));
    box.setButtonText (QMessageBox::Cancel,QString::fromLocal8Bit("取 消"));
    if(box.exec() != QMessageBox::Ok)
    {
        event->ignore();
    }*/

    int state = CMessageBox::Question(this,
                                      QString::fromLocal8Bit("退出提示"),
                                                          QString::fromLocal8Bit("您确定要退出")+
                                                          IDD_MAINDLG_TITLE+
                                                          IDD_MAINDLG_VERSION+
                                                          QString::fromLocal8Bit("?"));
    if(state != QMessageBox::Yes)
    {
        event->ignore();
    }

    SavaAllLabels();
}

void MainDialog::on_pushButton_2_clicked()
{
    this->close();
}

void MainDialog::on_pushButton_4_clicked()
{
    this->showMinimized();
}

void MainDialog::on_pushButton_3_clicked()
{
    if(!m_isDialogMax)
        this->showMaximized();
    else
        this->showNormal();

    m_isDialogMax = !m_isDialogMax;
}

/**
 * @brief MainDialog::signaldeleteNavigationForm 删除页面标签
 * @param form
 */
void MainDialog::signaldeleteNavigationForm(NavigationForm *form)
{
    if(form == NULL || m_NavigationForms.size() <= 1)
        return;

    int tmpDecSelected = -1;

    for(int i=0;i<m_NavigationForms.size();i++)
    {
        if(m_NavigationForms[i] != form)
            continue;

        disconnect(m_NavigationForms[i],
                SIGNAL(signaldeleteself(NavigationForm*)),
                this,
                SLOT(signaldeleteNavigationForm(NavigationForm*)));
        disconnect(m_NavigationForms[i],
                SIGNAL(signalopennewlab(NavigationForm*,const QUrl &)),
                this,
                SLOT(onprocesssignalopennewlab(NavigationForm*,const QUrl &)));

        disconnect(m_NavigationForms[i]->getWebEngineView(),
                SIGNAL(loadFinished(bool)),
                this,
                SLOT(onprocessWebloadFinished(bool)));
        disconnect(m_NavigationForms[i]->getWebEngineView(),
                SIGNAL(loadProgress(int)),
                this,
                SLOT(onprocessWebloadProgress(int)));
        disconnect(m_NavigationForms[i]->getWebEngineView(),
                SIGNAL(loadStarted()),
                this,
                SLOT(onprocessWebloadStarted()));
        disconnect(m_NavigationForms[i]->getWebEngineView(),
                SIGNAL(iconChanged(const QIcon &)),
                this,
                SLOT(onprocessWebiconChanged(const QIcon &)));
        disconnect(m_NavigationForms[i],
                SIGNAL(getHTML(QString)),
                this,
                SLOT(handleHTML(QString)));
        disconnect(m_NavigationForms[i],
                SIGNAL(signalCopyClipboard()),
                this,
                SLOT(OnMenuTriggerCopyClipboard()));
        disconnect(m_NavigationForms[i],
                SIGNAL(signalDownloadLink()),
                this,
                SLOT(OnMenuTriggerDownloadLink()));
        disconnect(m_NavigationForms[i],
                SIGNAL(signalOpenLinkInNewWindow()),
                this,
                SLOT(OnMenuTriggerOpenLinkInNewWindow()));
        disconnect(m_NavigationForms[i],
                SIGNAL(signalOpenLinkInNewTab()),
                this,
                SLOT(OnMenuTriggerOpenLinkInNewTab()));

        if(m_NavigationForms[i] &&
           m_NavigationForms[i]->getWebEngineView()->page())
        {
            disconnect(m_NavigationForms[i]->getWebEngineView()->page(),
                    SIGNAL(linkHovered(const QString &)),
                    this,
                    SLOT(onprocessWeblinkHovered(const QString &)));
        }

        ui->horizontalLayout_4->removeWidget(m_NavigationForms[i]);
        m_NavigationForms[i]->deleteLater();

        tmpDecSelected = i;

        m_NavigationForms.remove(i);
        break;
    }

    if(tmpDecSelected >= m_NavigationForms.size())
        tmpDecSelected = m_NavigationForms.size()-1;

    if(m_currentNavigationForm != NULL)
    {
        ui->horizontalLayout_6->removeWidget(m_currentNavigationForm->getWebEngineView());
        m_currentNavigationForm->getWebEngineView()->hide();
    }

    m_currentNavigationForm = m_NavigationForms[tmpDecSelected];
    m_currentNavigationForm->setSelected(true);
    m_ToolBarForm->setCurrentUrl(m_currentNavigationForm->getUrl());

    ui->horizontalLayout_6->addWidget(m_currentNavigationForm->getWebEngineView());
    m_currentNavigationForm->getWebEngineView()->show();
}

/**
 * @brief MainDialog::addNewLabel 添加一个新的标签页
 * @param title
 */
void MainDialog::addNewLabel(QString title,bool isfirstload,int positon,QUrl decUrl)
{
    // 计算所有控件的长度
    int totalLenght = 0;

    for(int i=0;i<m_NavigationForms.size();i++)
    {
        m_NavigationForms[i]->setSelected(false);
        totalLenght += m_NavigationForms[i]->size().width();
    }
    totalLenght += m_addNewLabelBtn->size().width();

    if(totalLenght + IDD_LABEL_WIDTH > this->size().width() - 100)
        return;

    NavigationForm *pNavigationForm = new NavigationForm(this);
    pNavigationForm->setTitle(title,QUrl(""));
    pNavigationForm->setFixedWidth(IDD_LABEL_WIDTH);
    pNavigationForm->setSelected(true);

    this->setWindowTitle(IDD_MAINDLG_TITLE+
                         IDD_MAINDLG_VERSION+
                         "-"+
                         title);

    connect(pNavigationForm,
            SIGNAL(signaldeleteself(NavigationForm*)),
            this,
            SLOT(signaldeleteNavigationForm(NavigationForm*)));
    connect(pNavigationForm,
            SIGNAL(signalopennewlab(NavigationForm*,const QUrl &)),
            this,
            SLOT(onprocesssignalopennewlab(NavigationForm*,const QUrl &)));

    connect(pNavigationForm->getWebEngineView(),
            SIGNAL(loadFinished(bool)),
            this,
            SLOT(onprocessWebloadFinished(bool)));
    connect(pNavigationForm->getWebEngineView(),
            SIGNAL(loadProgress(int)),
            this,
            SLOT(onprocessWebloadProgress(int)));
    connect(pNavigationForm->getWebEngineView(),
            SIGNAL(loadStarted()),
            this,
            SLOT(onprocessWebloadStarted()));
    connect(pNavigationForm->getWebEngineView(),
            SIGNAL(iconChanged(const QIcon &)),
            this,
            SLOT(onprocessWebiconChanged(const QIcon &)));
    connect(pNavigationForm,
            SIGNAL(getHTML(QString)),
            this,
            SLOT(handleHTML(QString)),
            Qt::QueuedConnection);
    connect(pNavigationForm,
            SIGNAL(signalCopyClipboard()),
            this,
            SLOT(OnMenuTriggerCopyClipboard()));
    connect(pNavigationForm,
            SIGNAL(signalDownloadLink()),
            this,
            SLOT(OnMenuTriggerDownloadLink()));
    connect(pNavigationForm,
            SIGNAL(signalOpenLinkInNewWindow()),
            this,
            SLOT(OnMenuTriggerOpenLinkInNewWindow()));
    connect(pNavigationForm,
            SIGNAL(signalOpenLinkInNewTab()),
            this,
            SLOT(OnMenuTriggerOpenLinkInNewTab()));

    pNavigationForm->installEventFilter(this);

    if(ui->horizontalLayout_4->count() == 0)
        ui->horizontalLayout_4->addWidget(pNavigationForm);
    else
        ui->horizontalLayout_4->insertWidget((positon == -1 ?
                                                  ui->horizontalLayout_4->count()-2 : positon),
                                             pNavigationForm);

    if(isfirstload)
    {
        ui->horizontalLayout_4->addWidget(m_addNewLabelBtn);
        ui->horizontalLayout_4->addStretch();
    }

    m_NavigationForms.push_back(pNavigationForm);

    m_ToolBarForm->setCurrentUrl(QUrl(""));

    if(m_currentNavigationForm != NULL)
    {
        ui->horizontalLayout_6->removeWidget(m_currentNavigationForm->getWebEngineView());
        m_currentNavigationForm->getWebEngineView()->hide();
    }

    m_currentNavigationForm = pNavigationForm;

    ui->horizontalLayout_6->addWidget(m_currentNavigationForm->getWebEngineView());
    m_currentNavigationForm->getWebEngineView()->show();

    m_currentNavigationForm->showDefaultPage();
    m_ToolBarForm->setLabelChecked(false);

    if(!decUrl.isEmpty())
    {
        if(m_ToolBarForm) m_ToolBarForm->setCurrentUrl(decUrl);
        if(m_currentNavigationForm)
        {
            m_currentNavigationForm->showPage(decUrl);
            m_ToolBarForm->setLabelChecked(this->isLabelExist(m_currentNavigationForm->getUrl().toString()));

            addNewLog(title,decUrl.toString());
        }
    }
}

/**
 * @brief MainDialog::addNewLog
 * @param name
 * @param url
 */
void MainDialog::addNewLog(QString name,QString url)
{
    if(name.isEmpty() || url.isEmpty())
        return;

    if(url.at(url.size()-1) == '/')
        url = url.mid(0,url.size()-1);

    if(url == name)
        return;

    bool isFinded = false;
    QVector<tagLog>::iterator iter = m_allLogs.begin();
    for(int i=0;iter != m_allLogs.end();++iter,i++)
    {
        if((*iter).name == name || (*iter).url == url)
        {
            isFinded = true;
            m_allLogs.erase(iter);
            break;
        }
    }

    if(m_allLogs.size() > 200)
        m_allLogs.pop_front();

    m_allLogs.push_back(tagLog(name,url));
}

/**
 * @brief MainDialog::SavaAllLabels 保存所有的标签
 */
void MainDialog::SavaAllLabels(void)
{
    QFile file(QCoreApplication::applicationDirPath()+"/data/systemset.xml");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
        return;

    //QMutexLocker tempLocker(&m_MutexAllnodes);

    QXmlStreamWriter xmlWriter(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement("params");

    xmlWriter.writeStartElement("labels");

    QVector<tagLabel>::iterator iter = m_allLabels.begin();
    for(;iter != m_allLabels.end();++iter)
    {
        xmlWriter.writeStartElement("label");
        xmlWriter.writeAttribute("name", (*iter).name);
        xmlWriter.writeAttribute("url", (*iter).url);
        xmlWriter.writeEndElement();
    }

    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement("logs");

    QVector<tagLog>::iterator iter2 = m_allLogs.begin();
    for(;iter2 != m_allLogs.end();++iter2)
    {
        xmlWriter.writeStartElement("log");
        xmlWriter.writeAttribute("name", (*iter2).name);
        xmlWriter.writeAttribute("url", (*iter2).url);
        xmlWriter.writeEndElement();
    }

    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
    xmlWriter.writeEndDocument();

    file.close();
}

/**
 * @brief MainDialog::LoadAllLabels 导入所有保存的标签
 */
void MainDialog::LoadAllLabels(void)
{
    QFile file(QCoreApplication::applicationDirPath()+"/data/systemset.xml");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Truncate | QIODevice::Text))
        return;

    m_allLabels.clear();
    m_allLogs.clear();

    QXmlStreamReader reader(&file);

    while(!reader.atEnd() && !reader.hasError())
    {
        if(reader.isStartElement())
        {
            if(reader.name() == "label")
            {
                QXmlStreamAttributes attributes = reader.attributes();

                QString name = attributes.value("name").toString();
                QString url = attributes.value("url").toString();

                if(!name.isEmpty() && !url.isEmpty())
                    m_allLabels.push_back(tagLabel(name,NULL,url));
            }
            else if(reader.name() == "log")
            {
                QXmlStreamAttributes attributes = reader.attributes();

                QString name = attributes.value("name").toString();
                QString url = attributes.value("url").toString();

                if(!name.isEmpty() && !url.isEmpty())
                    m_allLogs.push_back(tagLog(name,url));
            }

            reader.readNext();
            continue;
        }

        reader.readNextStartElement();
    }

    file.close();
}

/**
 * @brief MainDialog::deleteLableByIndex 删除指定索引的标签记录
 * @param index
 */
void MainDialog::deleteLableByIndex(int index)
{
    if(index<0||index>=m_allLabels.size())
        return;

    if(m_allLabels[index].label)
    {
        ui->horizontalLayout_5->removeWidget(m_allLabels[index].label);
        m_allLabels[index].label->deleteLater();
    }

    m_allLabels.remove(index);
}

/**
 * @brief MainDialog::SavaLabelRecords 保存标签记录
 */
void MainDialog::SavaLabelRecords(QString title,QString url,bool isSava)
{
    bool isFinded = false;
    int tmpIndex = -1;

    for(int i=0;i<m_allLabels.size();i++)
    {
        if(m_allLabels[i].url != url)
            continue;

        isFinded = true;
        tmpIndex = i;
        break;
    }

    if(isSava)
    {
        if(!isFinded)
        {
            if(m_allLabels.size() > 100)
                m_allLabels.pop_front();

            m_allLabels.push_back(tagLabel(title,NULL,url));
        }
    }
    else
    {
        if(isFinded && tmpIndex >= 0)
        {
            if(m_allLabels[tmpIndex].label)
            {
                ui->horizontalLayout_5->removeWidget(m_allLabels[tmpIndex].label);
                m_allLabels[tmpIndex].label->deleteLater();
            }

            m_allLabels.remove(tmpIndex);
        }
    }

    updateAllLabel();
}

/**
 * @brief MainDialog::updateAllLabel 更新所有的标签控件
 */
void MainDialog::updateAllLabel(void)
{
    SavaAllLabels();
    reloadAllLabelControls();
}

/**
 * @brief MainDialog::reloadAllLabelControls 加载所有的标签控件
 */
void MainDialog::reloadAllLabelControls(bool isfirstload)
{
    if(!m_allLabels.isEmpty())
    {
        QVector<tagLabel>::iterator iter = m_allLabels.begin();
        for(;iter != m_allLabels.end();++iter)
        {
            if((*iter).label == NULL)
                continue;

            ui->horizontalLayout_5->removeWidget((*iter).label);
            (*iter).label->deleteLater();
        }
    }

    QVector<QToolButton*> tmpBtns;

    for(int i=m_allLabels.size()-1;i>=0;i--)
    {
        QToolButton *newbutton = new QToolButton(this);
        newbutton->setMaximumWidth(200);
        newbutton->setObjectName(m_allLabels[i].name);
        newbutton->setToolTip(m_allLabels[i].name+
                              "\n"+
                              m_allLabels[i].url);
        newbutton->setText(m_allLabels[i].name);
        newbutton->setIcon(QIcon(":resources/bookmark-line.png"));
        newbutton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

        tmpBtns.push_front(newbutton);
    }

    for(int i=0,j=m_allLabels.size()-1;i<tmpBtns.size();i++,j--)
    {
        tmpBtns[i]->installEventFilter(this);

        m_allLabels[j].label = tmpBtns[i];

        if(ui->horizontalLayout_5->count() == 0)
            ui->horizontalLayout_5->addWidget(tmpBtns[i]);
        else
            ui->horizontalLayout_5->insertWidget(0,tmpBtns[i]);
    }

    if(isfirstload)
        ui->horizontalLayout_5->addStretch();
}

void MainDialog::onprocessaddNewLabelBtn(void)
{
    addNewLabel();
}

bool MainDialog::eventFilter(QObject *watched, QEvent *event)
{
    if(watched == this)
    {
        switch (event->type())
        {
        case QEvent::KeyPress:
        {
            QKeyEvent *pKeyEvent = static_cast<QKeyEvent*>(event);

            if(pKeyEvent)
            {
                if(pKeyEvent->key() == Qt::Key_Return)
                {
                    QUrl tmpurl = m_ToolBarForm->getCurrentInputUrl();

                    if(!tmpurl.isEmpty())
                    {
                        if(m_ToolBarForm) m_ToolBarForm->setCurrentUrl(tmpurl);
                        if(m_currentNavigationForm)
                        {
                            m_currentNavigationForm->showPage(tmpurl);
                            m_ToolBarForm->setLabelChecked(this->isLabelExist(m_currentNavigationForm->getUrl().toString()));

                            addNewLog(m_currentNavigationForm->getTitle(),tmpurl.toString());
                        }
                    }
                }
                else if(pKeyEvent->key() == Qt::Key_F5)
                {
                    if(m_currentNavigationForm)
                        m_currentNavigationForm->reload();
                }
                else if(pKeyEvent->key() == Qt::Key_F11)
                {
                    this->SshowFullScreen(!this->isFullScreen());
                }
            }
        }
            break;
        default:
            break;
        }
    }

    if(event->type() == QEvent::MouseButtonPress)
    {
        QVector<tagLabel>::iterator iter = m_allLabels.begin();
        for(;iter != m_allLabels.end();++iter)
        {
            if((*iter).name == watched->objectName())
            {
                if(m_currentNavigationForm)
                    m_currentNavigationForm->setTitle("",QUrl((*iter).url));
                if(m_ToolBarForm)
                    m_ToolBarForm->setCurrentUrl(QUrl((*iter).url));
                if(m_currentNavigationForm)
                {
                    m_currentNavigationForm->showPage(QUrl((*iter).url));
                    m_ToolBarForm->setLabelChecked(this->isLabelExist(m_currentNavigationForm->getUrl().toString()));

                    addNewLog((*iter).name,(*iter).url);
                }

                break;
            }
        }
    }

    if(event->type() == QEvent::MouseButtonPress)
    {
        // 处理用户选择页面标签
        onprocessUserSelectedPage(static_cast<NavigationForm*>(watched),
                                  event->type());

        if(m_LastLogsForm &&
           m_LastLogsForm->isVisible() &&
           watched != m_LastLogsForm)
        {
            m_LastLogsForm->setVisible(false);
        }
    }

    if(event->type() == QEvent::Leave ||
       event->type() == QEvent::Enter)
    {
        if(m_LastLogsForm &&
           m_LastLogsForm->isVisible() &&
           watched != m_LastLogsForm)
        {
            m_LastLogsForm->setVisible(false);
        }
    }

    if(event->type() == QEvent::Wheel &&
       watched == ui->scrollArea)
    {
        QWheelEvent *pWheelEvent = static_cast<QWheelEvent*>(event);

        if(pWheelEvent)
        {
            ui->scrollArea->horizontalScrollBar()->setValue(pWheelEvent->delta());
        }
    }

    return FramelessWindow::eventFilter(watched,event);
}

void MainDialog::onWebEngineViewtitleChanged(const QString &title)
{
    if(title.contains("default.html"))
        return;

    if(m_currentNavigationForm)
    {
        m_currentNavigationForm->setTitle(title,QUrl(""));

        this->updateUrlTitle(m_currentNavigationForm->getUrl().toString(),
                             title);

        this->setWindowTitle(IDD_MAINDLG_TITLE+
                             IDD_MAINDLG_VERSION+
                             "-"+
                             title);
    }
}

void MainDialog::onWebEngineViewurlChanged(const QUrl &url)
{
    if(url.toString().contains("file:///") || url.isEmpty())
        return;

    if(m_currentNavigationForm)
    {
        m_currentNavigationForm->setTitle("",url);
        m_currentNavigationForm->setCurrentUrl(url);
    }
    if(m_ToolBarForm) m_ToolBarForm->setCurrentUrl(url);
}

/**
 * @brief MainDialog::onprocessUserSelectedPage 处理用户选中页面标签
 * @param form
 */
void MainDialog::onprocessUserSelectedPage(NavigationForm *form,int eventType)
{
    if(form == NULL) return;

    QVector<NavigationForm*>::iterator iter = qFind(m_NavigationForms.begin(),
                                                    m_NavigationForms.end(),
                                                    form);
    if(iter == m_NavigationForms.end())
        return;

    if(m_currentNavigationForm != NULL)
    {
        ui->horizontalLayout_6->removeWidget(m_currentNavigationForm->getWebEngineView());
        m_currentNavigationForm->getWebEngineView()->hide();
    }

    m_currentNavigationForm = (*iter);

    ui->horizontalLayout_6->addWidget(m_currentNavigationForm->getWebEngineView());
    m_currentNavigationForm->getWebEngineView()->show();

    for(int i=0;i<m_NavigationForms.size();i++)
        m_NavigationForms[i]->setSelected(false);

    (*iter)->setSelected(true);    

    onprocesssignalOpenUrl(m_currentNavigationForm->getUrl());

    this->setWindowTitle(IDD_MAINDLG_TITLE+
                         IDD_MAINDLG_VERSION+
                         "-"+
                         (*iter)->getTitle());
}

void MainDialog::boxlayoutVisible(QBoxLayout *boxLayout, bool bVisible)
{
    if (NULL == boxLayout) return;

    int nColum = boxLayout->count();
    for (int i = 0; i < nColum; i++)
    {
        auto item = boxLayout->itemAt(i);
        if (item && item->widget())
        {
            item->widget()->setVisible(bVisible);
        }
    }
}

/**
 * @brief MainDialog::onprocesssignaltoLastPage 上一页
 */
void MainDialog::onprocesssignaltoLastPage(void)
{
    if(m_currentNavigationForm)
        m_currentNavigationForm->back();
}

/**
 * @brief MainDialog::onprocesssignaltoNextPage 下一页
 */
void MainDialog::onprocesssignaltoNextPage(void)
{
    if(m_currentNavigationForm)
        m_currentNavigationForm->forward();
}

/**
 * @brief MainDialog::onprocesssignalreloadPage 重新加载页面
 */
void MainDialog::onprocesssignalreloadPage(void)
{
    if(m_currentNavigationForm)
        m_currentNavigationForm->reload();
}

/**
 * @brief MainDialog::OnMenuTriggerOpenLinkInNewTab 从一个新的标签页打开
 * @param checked
 */
void MainDialog::OnMenuTriggerOpenLinkInNewTab(void)
{
    if(m_LoadStateForm == NULL)
        return;

    addNewLabel(QString::fromLocal8Bit("新标签页"),
                false,
                -1,
                m_LoadStateForm->getTitle());
}

/**
 * @brief MainDialog::OnMenuTriggerOpenLinkInNewWindow 从一个新的标签页打开
 */
void MainDialog::OnMenuTriggerOpenLinkInNewWindow(void)
{
    if(m_LoadStateForm == NULL)
        return;

    QProcess process(this);
    QString tmpPath = QApplication::applicationDirPath() +
                        "/smoothbrowser.exe "+
                        m_LoadStateForm->getTitle();

    process.startDetached(tmpPath);
}

/**
 * @brief MainDialog::OnMenuTriggerDownloadLink 下载连接
 */
void MainDialog::OnMenuTriggerDownloadLink(void)
{
    if(m_LoadStateForm == NULL)
        return;

    QString mCurOpertorFile = QFileDialog::getSaveFileName(this,QString::fromLocal8Bit("保存文件"),"./",
                                                          QString::fromLocal8Bit("所有文件(*.*);;文本文件(*.txt)"));

    QFile file(mCurOpertorFile);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
        return;

    QTextStream out(&file);
    out << m_LoadStateForm->getTitle();

    file.close();

    this->showTip(QString::fromLocal8Bit("连接保存成功,保存地址:")+mCurOpertorFile,2000);
}

/**
 * @brief MainDialog::OnMenuTriggerCopyClipboard 拷贝连接到剪切板
 * @param checked
 */
void MainDialog::OnMenuTriggerCopyClipboard(void)
{
    if(m_LoadStateForm == NULL)
        return;

    QString source = m_LoadStateForm->getTitle();
    QClipboard *clipboard = QApplication::clipboard();   //获取系统剪贴板指针
    clipboard->setText(source);                     //设置剪贴板内容</span>

    this->showTip(QString::fromLocal8Bit("连接已经拷贝到粘贴板中:")+
                  source,
                  2000);
}

/**
 * @brief MainDialog::onprocesssignalSaveLabel 保存标签
 */
void MainDialog::onprocesssignalSaveLabel(bool isSava)
{
    if(m_currentNavigationForm == NULL)
        return;

    this->SavaLabelRecords(m_currentNavigationForm->getTitle(),
                           m_currentNavigationForm->getUrl().toString(),
                           isSava);
}

/**
 * @brief MainDialog::onprocesssignalOpenUrl 打开页面
 * @param url
 */
void MainDialog::onprocesssignalOpenUrl(QUrl url)
{
    if(m_currentNavigationForm)
        m_currentNavigationForm->setTitle("",
                                          url);
    if(m_ToolBarForm)
        m_ToolBarForm->setCurrentUrl(url);

    if(url.isEmpty())
    {
        if(m_currentNavigationForm)
            m_currentNavigationForm->showDefaultPage();

        m_ToolBarForm->setLabelChecked(false);
    }
    else
    {
        if(m_currentNavigationForm)
        {
            m_currentNavigationForm->showPage(url);
            m_ToolBarForm->setLabelChecked(this->isLabelExist(m_currentNavigationForm->getUrl().toString()));

            addNewLog(m_currentNavigationForm->getTitle(),url.toString());
        }
    }
}

/**
 * @brief MainDialog::isLabelExist 检测指定标签是否存在
 * @param url
 * @return
 */
bool MainDialog::isLabelExist(QString url)
{
    QVector<tagLabel>::iterator iter = m_allLabels.begin();
    for(;iter != m_allLabels.end();++iter)
    {
        if((*iter).url != url)
            continue;

        return true;
    }

    return false;
}

/**
 * @brief MainDialog::onprocesssignalopennewlab 打开一个新的标签页
 * @param url
 */
void MainDialog::onprocesssignalopennewlab(NavigationForm* tabIndex,const QUrl &url)
{
    if(tabIndex == NULL || url.isEmpty()) return;

    int tmpIndex = -1;

    for(int i=0;i<m_NavigationForms.size();i++)
    {
        if(m_NavigationForms[i] == tabIndex)
        {
            tmpIndex = i;
            break;
        }
    }

    if(tmpIndex < 0)
        return;

    this->addNewLabel(QString::fromLocal8Bit("新标签页"),
                      false,
                      tmpIndex+1,
                      url);
}

void MainDialog::on_pushButton_clicked()
{
    QSize formSize = m_LastLogsForm->size();

    m_LastLogsForm->setGeometry(QRect(QPoint(ui->pushButton->geometry().bottomRight().x()-formSize.width()+9,
                                             ui->pushButton->geometry().bottomRight().y()-9),
                                      formSize));
    m_LastLogsForm->setVisible(!m_LastLogsForm->isVisible());

    if(m_LastLogsForm->isVisible())
    {
        m_LastLogsForm->clearAllData();

        for(int i=0;i<m_allLogs.size();i++)
        {
             m_LastLogsForm->addLog(m_allLogs[i].name,
                                    m_allLogs[i].url);
        }

        m_LastLogsForm->addSpace();

        m_LastLogsForm->activateWindow();
        m_LastLogsForm->raise();
    }
}

/**
 * @brief MainDialog::onprocessSelectedCurrentItem 用户选中选项
 * @param name
 * @param url
 */
void MainDialog::onprocessSelectedCurrentItem(QString name,QString url)
{
    this->addNewLabel(name,
                      false,
                      -1,
                      url);
}

/**
 * @brief MainDialog::onprocessDeleteCurrentItem 处理删除当前选项
 * @param name
 * @param url
 */
void MainDialog::onprocessDeleteCurrentItem(QString name,QString url)
{
    this->deleteLog(name,url);
}

/**
 * @brief MainDialog::updateUrlTitle 更改指定url的标题
 * @param url
 * @param title
 */
void MainDialog::updateUrlTitle(QString url,QString title)
{
    if(url.isEmpty() ||
       title.isEmpty())
        return;

    if(url.at(url.size()-1) == '/')
        url = url.mid(0,url.size()-1);

    if(url == title)
        return;

    for(int i=0;i<m_allLogs.size();i++)
    {
        if(m_allLogs[i].url == url)
        {
            m_allLogs[i].name = title;
            break;
        }
    }
}

/**
 * @brief MainDialog::deleteLog 删除指定标题的浏览记录
 * @param name
 * @param url
 */
void MainDialog::deleteLog(QString name,QString url)
{
    if(name.isEmpty() || url.isEmpty())
        return;

    for(int i=0;i<m_allLogs.size();i++)
    {
        if(m_allLogs[i].url == url && m_allLogs[i].name == name)
        {
            m_allLogs.remove(i);
            break;
        }
    }
}

/**
 * @brief MainDialog::onprocessSystemSetMenu 系统设置菜单
 */
void MainDialog::onprocessSystemSetMenu(void)
{
    m_Menu.popup(QCursor::pos());
}

/**
 * @brief MainDialog::OnSetMenuTriggered 处理设置菜单消息
 * @param action
 */
void MainDialog::OnSetMenuTriggered(QAction* action)
{
    if(action->text() == QString::fromLocal8Bit("退出"))
    {
        this->close();
    }
    else if(action->text() == QString::fromLocal8Bit("关于"))
    {
        m_AboatDialog->setParam(IDD_MAINDLG_TITLE,IDD_MAINDLG_VERSION);
        m_AboatDialog->exec();
    }
    else if(action->text() == QString::fromLocal8Bit("打开新的标签页"))
    {
        this->addNewLabel();
    }
    else if(action->text() == QString::fromLocal8Bit("打开新的窗口"))
    {
        QProcess process(this);
        process.startDetached(QCoreApplication::applicationDirPath()+
                              "/smoothbrowser.exe");
    }
    else if(action->text() == QString::fromLocal8Bit("打印"))
    {
        if(m_currentNavigationForm)
        {
            m_currentWebEnginePage = m_currentNavigationForm->getWebEngineView()->page();

            QPrinter printer;
            QPrintPreviewDialog preview(&printer,
                                        m_currentWebEnginePage->view());
            connect(&preview, &QPrintPreviewDialog::paintRequested,
                  this, &MainDialog::printDocument);
            preview.exec();
        }
    }
    else if(action->text() == QString::fromLocal8Bit("源代码"))
    {
        if(m_currentNavigationForm != NULL &&
           !m_currentNavigationForm->getUrl().toString().contains("default.html"))
        {
            m_currentNavigationForm->showPageSource();
        }
    }
    else if(action->text() == QString::fromLocal8Bit("历史记录"))
    {
        m_MsgManagerDialog->ShowPage(0);
    }
    else if(action->text() == QString::fromLocal8Bit("标签管理"))
    {
        m_MsgManagerDialog->ShowPage(1);
    }
}

/**
 * @brief MainDialog::onprocessWebloadFinished 处理网页加载完成
 * @param ok
 */
void MainDialog::onprocessWebloadFinished(bool ok)
{
    m_LoadStateForm->hide();

    if(m_currentNavigationForm &&
       m_currentNavigationForm->getWebEngineView()->page())
    {
        connect(m_currentNavigationForm->getWebEngineView()->page(),
                SIGNAL(linkHovered(const QString &)),
                this,
                SLOT(onprocessWeblinkHovered(const QString &)));
    }
}

/**
 * @brief MainDialog::onprocessWebloadProgress 处理网页加载中
 * @param progress
 */
void MainDialog::onprocessWebloadProgress(int progress)
{
    if(!m_LoadStateForm->isVisible())
        return;

    QString tmpString = m_LoadStateForm->getTitle()+
                                    QString::asprintf(" (%d%c)",progress,'%');

    QFontMetrics fontMetrics(m_LoadStateForm->font());
    int nFontWidth = fontMetrics.width(tmpString);

    QSize tmpStateFormSize = m_LoadStateForm->size();
    m_LoadStateForm->setGeometry(QRect(QPoint(2,this->size().height()-tmpStateFormSize.height()),
                                       QSize(nFontWidth,tmpStateFormSize.height())));

    m_LoadStateForm->setTitle(tmpString,
                              false);

    if(progress >= 100)
        m_LoadStateForm->hide();
}

/**
 * @brief MainDialog::onprocessWebloadStarted 处理网页开始加载中
 */
void MainDialog::onprocessWebloadStarted()
{
    if(m_currentNavigationForm == NULL &&
       !m_currentNavigationForm->getUrl().toString().isEmpty())
        return;

    QString tmpString = m_currentNavigationForm->getUrl().toString();

    QFontMetrics fontMetrics(m_LoadStateForm->font());
    int nFontWidth = fontMetrics.width(tmpString);
    int nFontMaxWidth = this->size().width()/2;

    if(nFontWidth > nFontMaxWidth)
    {
        tmpString = fontMetrics.elidedText(tmpString,Qt::ElideRight,nFontMaxWidth);
        nFontWidth = nFontMaxWidth;
    }

    m_LoadStateForm->setTitle(tmpString);
    m_LoadStateForm->activateWindow();

    QSize tmpStateFormSize = m_LoadStateForm->size();
    m_LoadStateForm->setGeometry(QRect(QPoint(2,this->size().height()-tmpStateFormSize.height()),
                                       QSize(nFontWidth,tmpStateFormSize.height())));

    m_LoadStateForm->raise();
    m_LoadStateForm->show();
}

void MainDialog::handleHTML(QString sHTML)
{
    if(m_currentNavigationForm == NULL ||
       m_currentNavigationForm->getUrl().toString() == "" ||
       m_currentNavigationForm->getUrl().toString().contains("default.html"))
        return;

    m_ShowPageSourceDialog->setWindowTitle(QString::fromLocal8Bit("源代码-")+
                                           m_currentNavigationForm->getTitle());
    m_ShowPageSourceDialog->showcontent(sHTML);
    m_ShowPageSourceDialog->show();
}

/**
 * @brief MainDialog::onprocessWeblinkHovered 处理网页用户鼠标移动到超链接上
 * @param url
 */
void MainDialog::onprocessWeblinkHovered(const QString &url)
{
    if(url == "" && m_LoadStateForm->isVisible())
    {
        m_LoadStateForm->hide();
        return;
    }

    if(m_currentNavigationForm == NULL ||
       url == "")
        return;

    QString tmpString = url;

    QFontMetrics fontMetrics(m_LoadStateForm->font());
    int nFontWidth = fontMetrics.width(tmpString);
    int nFontMaxWidth = this->size().width()/2;

    if(nFontWidth > nFontMaxWidth)
    {
        tmpString = fontMetrics.elidedText(tmpString,Qt::ElideRight,nFontMaxWidth);
        nFontWidth = nFontMaxWidth;
    }

    m_LoadStateForm->setTitle(tmpString);
    m_LoadStateForm->activateWindow();

    QSize tmpStateFormSize = m_LoadStateForm->size();
    m_LoadStateForm->setGeometry(QRect(QPoint(2,this->size().height()-tmpStateFormSize.height()),
                                       QSize(nFontWidth,tmpStateFormSize.height())));

    m_LoadStateForm->raise();
    m_LoadStateForm->show();
}

/**
 * @brief MainDialog::onprocessWebiconChanged 处理网页加载完图标
 * @param icon
 */
void MainDialog::onprocessWebiconChanged(const QIcon &icon)
{
    if(m_currentNavigationForm)
        m_currentNavigationForm->setIcon(icon);
}

void MainDialog::printDocument(QPrinter *printer)
{
    if(m_currentWebEnginePage == NULL)
        return;

    QEventLoop loop;
    bool result;
    auto printPreview = [&](bool success) { result = success; loop.quit(); };
    m_currentWebEnginePage->print(printer, std::move(printPreview));
    loop.exec();

    if (!result) {
     QPainter painter;
     if (painter.begin(printer)) {
         QFont font = painter.font();
         font.setPixelSize(20);
         painter.setFont(font);
         painter.drawText(QPointF(10,25),
                          QStringLiteral("Could not generate print preview."));

         painter.end();
     }
    }
}

