﻿#include "loadstateform.h"
#include "ui_loadstateform.h"

LoadStateForm::LoadStateForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoadStateForm)
{
    ui->setupUi(this);
}

LoadStateForm::~LoadStateForm()
{
    delete ui;
}

void LoadStateForm::setTitle(QString title,bool isSetSrc)
{
    if(title.isEmpty())
        return;

    if(isSetSrc) m_orgTitle = title;
    ui->label->setText(title);
}
