﻿#ifndef _MAINDIALOG_H_INCLUDE_
#define _MAINDIALOG_H_INCLUDE_

#include <QLabel>
#include <QToolButton>
#include <QHash>
#include <QPushButton>
#include <QBoxLayout>
#include <QMenu>
#include <QTimer>

#include "common/singleton.h"
#include "widgets/libframelesswindow.h"
#include "toolbarform.h"
#include "cwebengineview.h"
#include "navigationform.h"
#include "lastlogsform.h"
#include "aboatdialog.h"
#include "loadstateform.h"
#include "showpagesourcedialog.h"
#include "msgmanagerdialog.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainDialog; }
QT_END_NAMESPACE

#define IDD_MAINDLG_TITLE QString::fromLocal8Bit("畅畅浏览器")
#define IDD_MAINDLG_VERSION "v1.0.0"

struct tagLabel
{
    tagLabel()
        : label(NULL) {}
    tagLabel(QString n,QToolButton *l,QString u)
        : name(n),label(l),url(u) {}

    QString name;
    QToolButton *label;
    QString url;
};

struct tagLog
{
    tagLog(){}
    tagLog(QString n,QString u)
        : name(n),url(u) {}

    QString name;
    QString url;
};

class MainDialog : public FramelessWindow , public Singleton<MainDialog>
{
    Q_OBJECT

public:
    explicit MainDialog(QWidget *parent = nullptr,QUrl url=QUrl(""));
    ~MainDialog();

    /// 导入所有保存的标签
    void LoadAllLabels(void);
    /// 保存所有的标签
    void SavaAllLabels(void);
    /// 保存标签记录
    void SavaLabelRecords(QString title,QString url,bool isSava=true);
    /// 检测指定标签是否存在
    bool isLabelExist(QString url);
    /// 得到所有的日志
    inline QVector<tagLog>& getalllogs(void) { return m_allLogs; }
    /// 得到所有的标签
    inline QVector<tagLabel>& getalllabels(void) { return m_allLabels; }
    /// 添加一个新的标签页
    void addNewLabel(QString title=QString::fromLocal8Bit("新标签页"),
                     bool isfirstload=false,
                     int positon=-1,
                     QUrl decUrl=QUrl(""));
    /// 添加一个新的日志记录
    void addNewLog(QString name,QString url);
    /// 更改指定url的标题
    void updateUrlTitle(QString url,QString title);
    /// 删除指定标题的浏览记录
    void deleteLog(QString name,QString url);
    /// 删除指定索引的浏览记录
    inline void deleteLogByIndex(int index) { if(index<0||index>=m_allLogs.size()) return; m_allLogs.remove(index); }
    /// 删除指定索引的标签记录
    void deleteLableByIndex(int index);
    /// 隐藏显示布局
    void boxlayoutVisible(QBoxLayout *boxLayout, bool bVisible);
    /// 是否全屏显示
    void SshowFullScreen(bool isShow);
    /// 显示提示
    void showTip(QString tip,int time=-1);
    /// 更新所有的标签控件
    void updateAllLabel(void);

private:
    /// 加载所有的标签控件
    void reloadAllLabelControls(bool isfirstload=false);
    /// 处理用户选中页面标签
    void onprocessUserSelectedPage(NavigationForm *form,int eventType);

protected:
    void changeEvent(QEvent * event);
    void closeEvent(QCloseEvent *event);
    bool eventFilter(QObject *watched, QEvent *event);

private slots:
    void on_pushButton_2_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_clicked();
    void onprocessaddNewLabelBtn(void);
    /// 删除页面标签
    void signaldeleteNavigationForm(NavigationForm *form);
    void onWebEngineViewtitleChanged(const QString &title);
    void onWebEngineViewurlChanged(const QUrl &url);
    /// 上一页
    void onprocesssignaltoLastPage(void);
    /// 下一页
    void onprocesssignaltoNextPage(void);
    /// 重新加载页面
    void onprocesssignalreloadPage(void);
    /// 打开页面
    void onprocesssignalOpenUrl(QUrl url);
    /// 保存标签
    void onprocesssignalSaveLabel(bool isSava);
    /// 打开一个新的标签页
    void onprocesssignalopennewlab(NavigationForm* tabIndex,const QUrl &url);
    /// 处理删除当前选项
    void onprocessDeleteCurrentItem(QString name,QString url);
    /// 用户选中选项
    void onprocessSelectedCurrentItem(QString name,QString url);
    /// 系统设置菜单
    void onprocessSystemSetMenu(void);
    /// 处理设置菜单消息
    void OnSetMenuTriggered(QAction* action);
    /// 处理打印
    void printDocument(QPrinter *printer);

    void handleHTML(QString sHTML);
    void timerShowTip(void);
    /// 处理网页加载完成
    void onprocessWebloadFinished(bool ok);
    /// 处理网页加载中
    void onprocessWebloadProgress(int progress);
    /// 处理网页开始加载中
    void onprocessWebloadStarted();
    /// 处理网页加载完图标
    void onprocessWebiconChanged(const QIcon &icon);
    /// 处理网页用户鼠标移动到超链接上
    void onprocessWeblinkHovered(const QString &url);
    /// 拷贝连接到剪切板
    void OnMenuTriggerCopyClipboard(void);
    /// 下载连接
    void OnMenuTriggerDownloadLink(void);
    /// 从一个新的标签页打开
    void OnMenuTriggerOpenLinkInNewWindow(void);
    /// 从一个新的标签页打开
    void OnMenuTriggerOpenLinkInNewTab(void);

private:
    Ui::MainDialog *ui;
    bool m_isDialogMax;                         /**< 窗口是否最大化 */
    ToolBarForm *m_ToolBarForm;                 /**< 工具条 */
    QMenu m_Menu;                               /**< 系统设置菜单 */
    QTimer m_timerShowTip;                      /**< 显示提示定时器 */
    LastLogsForm *m_LastLogsForm;               /**< 最近的访问记录 */
    LoadStateForm *m_LoadStateForm;             /**< 加载状态栏 */
    AboatDialog *m_AboatDialog;                 /**< 关于对话框 */
    MsgManagerDialog *m_MsgManagerDialog;       /**< 消息记录管理对话框 */
    ShowPageSourceDialog *m_ShowPageSourceDialog;      /**< 显示页面源代码 */
    QVector<tagLabel> m_allLabels;              /**< 所有的标签 */
    QVector<tagLog> m_allLogs;                  /**< 所有的日志 */
    QPushButton *m_addNewLabelBtn;              /**< 添加新的标签 */
    QVector<NavigationForm*> m_NavigationForms; /**< 所有打开的标签页 */
    NavigationForm *m_currentNavigationForm;    /**< 当前操作的标签页 */
    QWebEnginePage *m_currentWebEnginePage;     /**< 当前操作页面 */
};

#endif // MAINDIALOG_H
