﻿#include "logitemform.h"
#include "ui_logitemform.h"

#include <QPainter>
#include <qDebug>

LogItemForm::LogItemForm(QString name,QString url,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LogItemForm),
    mName(name),
    mUrl(url),
    m_mouseOvered(false)
{
    ui->setupUi(this);

    this->setFocusPolicy(Qt::StrongFocus);
    this->installEventFilter(this);

    setParam(name,url);
}

LogItemForm::~LogItemForm()
{
    delete ui;
}

void LogItemForm::getParam(QString &name,QString &url)
{
    name = mName;
    url = mUrl;
}

void LogItemForm::setParam(QString name,QString url)
{
    if(name.isEmpty() || url.isEmpty())
        return;

    QFontMetrics fontMetrics(this->font());

    ui->label_3->setText(fontMetrics.elidedText(name,Qt::ElideRight,this->rect().width()-60));
    ui->label_2->setText(fontMetrics.elidedText(url,Qt::ElideRight,this->rect().width()-60));
}

void LogItemForm::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    if(!m_mouseOvered)
        painter.fillRect(0,0,this->width(),this->height(),QColor(236,233,216,0));
    else
        painter.fillRect(0,0,this->width(),this->height(),QColor(128,128,128,100));
}

bool LogItemForm::eventFilter(QObject *watched, QEvent *event)
{
    if(watched == this)
    {
        if((event->type() == QEvent::Enter ||
            event->type() == QEvent::Leave))
        {
            bool mouseOvered = (event->type() == QEvent::Enter ? true : false);

            if(mouseOvered != m_mouseOvered)
            {
                m_mouseOvered = mouseOvered;
                this->update();
            }
        }
        else if(event->type() == QEvent::MouseButtonPress)
        {
            emit signalSelectedCurrentItem(this,mName,mUrl);
        }
    }

    return QWidget::eventFilter(watched,event);
}

void LogItemForm::on_pushButton_clicked()
{
    emit signalDeleteCurrentItem(this,mName,mUrl);
}
