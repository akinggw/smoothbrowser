﻿#ifndef CWEBENGINEVIEW_H
#define CWEBENGINEVIEW_H

#include <QObject>
#include <QWebEngineView>
#include <QPrinter>
#include <QPrintPreviewDialog>

class CWebEngineView : public QWebEngineView
{
    Q_OBJECT

public:
    explicit CWebEngineView(QWidget *parent = nullptr);
    ~CWebEngineView();

    virtual QWebEngineView* createWindow(QWebEnginePage::WebWindowType type) override;

signals:
    /// 打开一个新的标签页
    void signalopennewlab(const QUrl &url);

private:
   QUrl m_url;
};

#endif // CWEBENGINEVIEW_H
