﻿#include "lastlogsform.h"
#include "ui_lastlogsform.h"

#include <QScrollArea>
#include <QScrollBar>
#include <QFontMetrics>

LastLogsForm::LastLogsForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LastLogsForm),
    m_isAddSpace(false)
{
    ui->setupUi(this);

    ui->scrollArea->verticalScrollBar()->setStyleSheet("QScrollBar {width:3px;}");
    ui->scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->scrollArea->verticalScrollBar()->setEnabled(true);
    ui->scrollArea->horizontalScrollBar()->setEnabled(false);
}

LastLogsForm::~LastLogsForm()
{
    delete ui;
}

void LastLogsForm::clearAllData(void)
{
    for(int i=0;i<m_LogItemForms.size();i++)
    {
        disconnect(m_LogItemForms[i],
                SIGNAL(signalDeleteCurrentItem(LogItemForm*,QString,QString)),
                this,
                SLOT(onprocessDeleteCurrentItem(LogItemForm*,QString,QString)));
        disconnect(m_LogItemForms[i],
                SIGNAL(signalSelectedCurrentItem(LogItemForm*,QString,QString)),
                this,
                SLOT(onprocessSelectedCurrentItem(LogItemForm*,QString,QString)));

        ui->verticalLayout_2->removeWidget(m_LogItemForms[i]);
        m_LogItemForms[i]->deleteLater();
    }

    m_LogItemForms.clear();
}

void LastLogsForm::addLog(QString name,QString url)
{
    if(name.isEmpty() || url.isEmpty())
        return;

    LogItemForm *pLogItemForm = new LogItemForm(name,url,this);
    pLogItemForm->setMaximumWidth(this->size().width()-20);

    connect(pLogItemForm,
            SIGNAL(signalDeleteCurrentItem(LogItemForm*,QString,QString)),
            this,
            SLOT(onprocessDeleteCurrentItem(LogItemForm*,QString,QString)));
    connect(pLogItemForm,
            SIGNAL(signalSelectedCurrentItem(LogItemForm*,QString,QString)),
            this,
            SLOT(onprocessSelectedCurrentItem(LogItemForm*,QString,QString)));

    QFontMetrics fontMetrics(this->font());
    int nFontWidth = fontMetrics.width(name);

    if(nFontWidth > this->size().width()-20)
        pLogItemForm->setToolTip(name);

    m_LogItemForms.push_back(pLogItemForm);

    ui->verticalLayout_2->insertWidget(0,pLogItemForm);
}

/**
 * @brief LastLogsForm::onprocessDeleteCurrentItem 处理删除当前选项
 * @param name
 * @param url
 */
void LastLogsForm::onprocessDeleteCurrentItem(LogItemForm *pform,QString name,QString url)
{
    QVector<LogItemForm*>::iterator iter = qFind(m_LogItemForms.begin(),
                                                 m_LogItemForms.end(),
                                                 pform);
    if(iter != m_LogItemForms.end())
    {
        (*iter)->deleteLater();
        m_LogItemForms.erase(iter);

        emit signalDeleteCurrentItem(name,url);
    }
}

/**
 * @brief LastLogsForm::onprocessSelectedCurrentItem 用户选中选项
 * @param pform
 * @param name
 * @param url
 */
void LastLogsForm::onprocessSelectedCurrentItem(LogItemForm *pform,QString name,QString url)
{
    emit signalSelectedCurrentItem(name,url);
}

void LastLogsForm::addSpace(void)
{
    if(!m_isAddSpace)
        ui->verticalLayout_2->addStretch();

    m_isAddSpace = true;
}
