﻿#include "msgmanagerdialog.h"
#include "ui_msgmanagerdialog.h"

#include "maindialog.h"
#include <QKeyEvent>
#include <qDebug>

MsgManagerDialog::MsgManagerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MsgManagerDialog),
    m_pageType(-1),
    m_BrowsingHistoryForm(NULL),
    m_LabelRecordForm(NULL),
    m_isMultSelected(false)
{
    ui->setupUi(this);

    this->installEventFilter(this);

    Qt::WindowFlags flags=Qt::Dialog;
    flags |=Qt::WindowCloseButtonHint;
    flags |=Qt::WindowMinimizeButtonHint;
    flags |=Qt::WindowMaximizeButtonHint;
    this->setWindowFlags(flags);

    m_BrowsingHistoryForm = new MsgRecordForm(this);
    m_LabelRecordForm = new MsgRecordForm(this);

    m_BrowsingHistoryForm->installEventFilter(this);
    m_LabelRecordForm->installEventFilter(this);

    ui->tabWidget->clear();
    ui->tabWidget->addTab(m_BrowsingHistoryForm,
                          QIcon(":/resources/database-2-line.png"),
                          QString::fromLocal8Bit("浏览记录管理"));
    ui->tabWidget->addTab(m_LabelRecordForm,
                          QIcon(":/resources/contacts-book-line.png"),
                          QString::fromLocal8Bit("标签管理"));
}

MsgManagerDialog::~MsgManagerDialog()
{
    delete ui;
    delete m_BrowsingHistoryForm;
    delete m_LabelRecordForm;
}

void MsgManagerDialog::ShowPage(int type)
{
    m_pageType = type;

    if(m_pageType == 0)
    {
        this->setWindowTitle(QString::fromLocal8Bit("浏览记录管理"));
        ui->tabWidget->setCurrentIndex(0);
        m_BrowsingHistoryForm->LoadRecordData(m_pageType);
    }
    else if(m_pageType == 1)
    {
        this->setWindowTitle(QString::fromLocal8Bit("标签管理"));
        ui->tabWidget->setCurrentIndex(1);
        m_LabelRecordForm->LoadRecordData(m_pageType);
    }

    this->exec();
}

void MsgManagerDialog::on_pushButton_clicked()
{
    if(m_pageType == 0)
        m_BrowsingHistoryForm->LoadRecordData(m_pageType,ui->lineEdit->text());
    else if(m_pageType == 1)
        m_LabelRecordForm->LoadRecordData(m_pageType,ui->lineEdit->text());
}

void MsgManagerDialog::on_pushButton_2_clicked()
{
    if(m_pageType == 0)
    {
        m_BrowsingHistoryForm->DeleteSelectedRecords();
        m_LabelRecordForm->LoadRecordData(m_pageType);
    }
    else if(m_pageType == 1)
    {
        m_LabelRecordForm->DeleteSelectedRecords();
        MainDialog::getSingleton().updateAllLabel();
    }
}

bool MsgManagerDialog::eventFilter(QObject *watched, QEvent *event)
{
    if(watched == m_BrowsingHistoryForm ||
       watched == m_LabelRecordForm)
    {
        switch (event->type())
        {
        case QEvent::KeyPress:
        case QEvent::KeyRelease:
        {
            QKeyEvent *pKeyEvent = static_cast<QKeyEvent*>(event);

            if(pKeyEvent)
            {
                if(pKeyEvent->key() == Qt::Key_Shift)
                {
                    m_isMultSelected = (event->type() == QEvent::KeyPress ? true : false);

                    if(m_pageType == 0)
                        m_BrowsingHistoryForm->setisMultSelected(m_isMultSelected);
                    else if(m_pageType == 1)
                        m_LabelRecordForm->setisMultSelected(m_isMultSelected);
                }
            }
        }
            break;
        default:
            break;
        }
    }

    return QDialog::eventFilter(watched,event);
}

void MsgManagerDialog::on_tabWidget_currentChanged(int index)
{

}

void MsgManagerDialog::on_tabWidget_tabBarClicked(int index)
{
    m_pageType = index;

    if(m_pageType == 0)
    {
        this->setWindowTitle(QString::fromLocal8Bit("浏览记录管理"));
        m_BrowsingHistoryForm->LoadRecordData(m_pageType);
    }
    else if(m_pageType == 1)
    {
        this->setWindowTitle(QString::fromLocal8Bit("标签管理"));
        m_LabelRecordForm->LoadRecordData(m_pageType);
    }
}
