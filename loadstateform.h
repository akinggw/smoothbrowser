﻿#ifndef LOADSTATEFORM_H
#define LOADSTATEFORM_H

#include <QWidget>

namespace Ui {
class LoadStateForm;
}

class LoadStateForm : public QWidget
{
    Q_OBJECT

public:
    explicit LoadStateForm(QWidget *parent = nullptr);
    ~LoadStateForm();

    void setTitle(QString title,bool isSetSrc=true);
    inline QString getTitle(void) { return m_orgTitle; }

private:
    Ui::LoadStateForm *ui;
    QString m_orgTitle;
};

#endif // LOADSTATEFORM_H
