﻿#include "toolbarform.h"
#include "ui_toolbarform.h"

#include <QMouseEvent>
#include <QTimer>

ToolBarForm::ToolBarForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ToolBarForm)
{
    ui->setupUi(this);

    this->installEventFilter(this);
    ui->lineEdit->installEventFilter(this);
}

ToolBarForm::~ToolBarForm()
{
    delete ui;
}

/**
 * @brief ToolBarForm::setCurrentUrl 设置当前要加载的网址
 * @param url
 */
void ToolBarForm::setCurrentUrl(QUrl url)
{
    m_curOperUrl = url;

    ui->lineEdit->setText(url.toString());
    //ui->lineEdit->setCursorPosition(0);

    if(url.isEmpty())
        ui->lineEdit->setFocus();
    else
        this->setFocus();
}

/**
 * @brief ToolBarForm::getCurrentInputUrl 得到当前输入的网址
 * @return
 */
QUrl ToolBarForm::getCurrentInputUrl(void)
{
    QString tmpUrlStr;
    if(!ui->lineEdit->text().isEmpty())
        tmpUrlStr = ui->lineEdit->text();

    if(!tmpUrlStr.isEmpty() && !tmpUrlStr.contains("http"))
        tmpUrlStr = ("https://" + tmpUrlStr);

    m_curOperUrl = QUrl(tmpUrlStr);

    return m_curOperUrl;
}

void ToolBarForm::on_pushButton_clicked()
{
    emit signaltoLastPage();
}

void ToolBarForm::on_pushButton_2_clicked()
{
    emit signaltoNextPage();
}

void ToolBarForm::on_pushButton_3_clicked()
{
    emit signalreloadPage();
}

void ToolBarForm::on_pushButton_4_clicked()
{
    if(ui->lineEdit->text().isEmpty())
        return;

    m_curOperUrl = QUrl(ui->lineEdit->text());

    emit signalOpenUrl(m_curOperUrl);
}

void ToolBarForm::on_pushButton_6_clicked()
{
    if(m_curOperUrl.isEmpty())
        return;

    emit signalSaveLabel(ui->pushButton_6->isChecked());
}

/**
 * @brief ToolBarForm::setLabelChecked 设置当前网址是否已经保存
 * @param checked
 */
void ToolBarForm::setLabelChecked(bool checked)
{
    if(m_curOperUrl.isEmpty())
        return;

    ui->pushButton_6->setChecked(checked);

    if(checked)
        ui->pushButton_6->setToolTip(QString::fromLocal8Bit("取消收藏"));
    else
        ui->pushButton_6->setToolTip(QString::fromLocal8Bit("收藏页面"));
}

void ToolBarForm::on_pushButton_5_clicked()
{
    emit signalSystemSetMenu();
}

bool ToolBarForm::eventFilter(QObject *obj, QEvent *e)
{
    if(obj == ui->lineEdit)
    {
       if(e->type() == QEvent::MouseButtonPress)
       {
            QMouseEvent *me=(QMouseEvent *)e;

            if(me->button() == Qt::LeftButton)//鼠标左键
            {
                //ui->lineEdit->selectAll();
                ui->lineEdit->setFocus();
            }
       }
       else if(e->type()==QEvent::FocusIn)
       {
            QTimer::singleShot( 50,[=]() mutable{
                ui->lineEdit->selectAll();
            });
       }
    }

    return QWidget::eventFilter(obj,e);
}

void ToolBarForm::on_lineEdit_cursorPositionChanged(int arg1, int arg2)
{

}
