﻿#include "maindialog.h"

#include <QApplication>

#include "QsLog/QsLog.h"
#include "common/common.h"
#include "breakpad/exception_handler.h"

bool my_dump_callback(const wchar_t *dump_path, const wchar_t *id, void *content, EXCEPTION_POINTERS *exinfo, MDRawAssertionInfo *assertion, bool succeeded)
{
    if(succeeded)
    {
        QLOG_ERROR()<<"系统已经崩溃,dmp已经生成成功,请联系开发人员!";

        MessageBoxA(NULL,
                    "系统已经崩溃,dmp已经生成成功,请联系开发人员!",
                    "警告",
                    MB_OK|MB_ICONWARNING);
    }
    else
    {
        QLOG_ERROR()<<"系统已经崩溃,dmp生成失败,请联系开发人员!";
    }

    return succeeded;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    google_breakpad::ExceptionHandler eh(
        L".", NULL, my_dump_callback, NULL, google_breakpad::ExceptionHandler::HANDLER_ALL);

    init_log_file("smoothbrowser_log.txt");

    MainDialog w(nullptr,argc >= 2 ? QUrl(argv[1]) : QUrl(""));
    w.show();

    return a.exec();
}
