﻿#include "cwebengineview.h"

#include <QAction>

CWebEngineView::CWebEngineView(QWidget *parent)
    : QWebEngineView(parent)
{
    this->pageAction(QWebEnginePage::Back)->setText(QStringLiteral("后退"));
    this->pageAction(QWebEnginePage::Forward)->setText(QStringLiteral("前进"));
    this->pageAction(QWebEnginePage::Reload)->setText(QStringLiteral("刷新"));
    this->pageAction(QWebEnginePage::SavePage)->setText(QStringLiteral("保存页面"));
    this->pageAction(QWebEnginePage::ViewSource)->setText(QStringLiteral("查看源码"));
    this->pageAction(QWebEnginePage::Undo)->setText(QStringLiteral("撤退"));
    this->pageAction(QWebEnginePage::Redo)->setText(QStringLiteral("重做"));
    this->pageAction(QWebEnginePage::Cut)->setText(QStringLiteral("剪切"));
    this->pageAction(QWebEnginePage::Copy)->setText(QStringLiteral("复制"));
    this->pageAction(QWebEnginePage::Paste)->setText(QStringLiteral("粘贴"));
    this->pageAction(QWebEnginePage::PasteAndMatchStyle)->setText(QStringLiteral("按样式粘贴"));
    this->pageAction(QWebEnginePage::SelectAll)->setText(QStringLiteral("全选"));

    this->pageAction(QWebEnginePage::OpenLinkInNewWindow)->setText(QStringLiteral("从新窗口打开"));
    this->pageAction(QWebEnginePage::OpenLinkInNewTab)->setText(QStringLiteral("从新标签页打开"));
    this->pageAction(QWebEnginePage::CopyLinkToClipboard)->setText(QStringLiteral("拷贝连接"));
    this->pageAction(QWebEnginePage::DownloadLinkToDisk)->setText(QStringLiteral("下载连接"));

    this->pageAction(QWebEnginePage::CopyImageToClipboard)->setText(QStringLiteral("拷贝图片"));
    this->pageAction(QWebEnginePage::CopyImageUrlToClipboard)->setText(QStringLiteral("拷贝图片地址"));
    this->pageAction(QWebEnginePage::DownloadImageToDisk)->setText(QStringLiteral("下载图片"));

    //TODO: 链接hover时改变m_url的值
    connect(this->page(), &QWebEnginePage::linkHovered, this, [this](const QString &url){
        m_url.setUrl(url);
    });
    connect(this->page(), &QWebEnginePage::urlChanged, this, [this](const QUrl &url){
        m_url = url;
    });
}

CWebEngineView::~CWebEngineView()
{

}

QWebEngineView *CWebEngineView::createWindow(QWebEnginePage::WebWindowType type)
{
    if(type == QWebEnginePage::WebBrowserTab)
    {
        emit signalopennewlab(m_url);

        return nullptr;
    }

    if(!m_url.isEmpty())
    {
        this->load(m_url);//加载新的url
    }

    return nullptr;
}
