﻿#ifndef LASTLOGSFORM_H
#define LASTLOGSFORM_H

#include <QWidget>
#include <QVector>

#include "logitemform.h"

namespace Ui {
class LastLogsForm;
}

class LastLogsForm : public QWidget
{
    Q_OBJECT

public:
    explicit LastLogsForm(QWidget *parent = nullptr);
    ~LastLogsForm();

    void addLog(QString name,QString url);
    void clearAllData(void);
    void addSpace(void);

signals:
    /// 删除当前选项
    void signalDeleteCurrentItem(QString name,QString url);
    /// 用户选中选项
    void signalSelectedCurrentItem(QString name,QString url);

private slots:
    /// 处理删除当前选项
    void onprocessDeleteCurrentItem(LogItemForm *pform,QString name,QString url);
    /// 用户选中选项
    void onprocessSelectedCurrentItem(LogItemForm *pform,QString name,QString url);

private:
    Ui::LastLogsForm *ui;
    QVector<LogItemForm*> m_LogItemForms;            /**< 所有的日志 */
    bool m_isAddSpace;
};

#endif // LASTLOGSFORM_H
