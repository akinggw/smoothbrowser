QT += core gui widgets
QT += network websockets
QT += concurrent winextras
QT += sql qml
QT += webenginewidgets
QT += printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

LIBS += -lGdi32 -luser32

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

#tcmalloc
#1.编译extends目录下gperftools-2.10.zip,编译成DLL模式，静态库模式测试有问题
#2.在extends目录下建立目录tcmalloc，然后拷贝libtcmalloc_minimal.lib到目录下,
#  libtcmalloc_minimal.dll拷贝到执行目录下
CONFIG(release,debug|release){
    QMAKE_LFLAGS_RELEASE += /INCLUDE:"__tcmalloc"
}else{
    QMAKE_LFLAGS_DEBUG += /INCLUDE:"__tcmalloc"
}

#Release编译,生成*.pdb调试文件
QMAKE_CXXFLAGS_RELEASE += $$QMAKE_CFLAGS_RELEASE_WITH_DEBUGINFO
QMAKE_LFLAGS_RELEASE += $$QMAKE_LFLAGS_RELEASE_WITH_DEBUGINFO

RC_ICONS = logo.ico

VERSION = 1.0.0.0

QMAKE_TARGET_PRODUCT = smoothbrowser
QMAKE_TARGET_COMPANY = abc.com
QMAKE_TARGET_DESCRIPTION = smoothbrowser
QMAKE_TARGET_COPYRIGHT = abc.com

DESTDIR = $$PWD/bin

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    aboatdialog.cpp \
    cwebengineview.cpp \
    lastlogsform.cpp \
    loadstateform.cpp \
    logitemform.cpp \
    main.cpp \
    maindialog.cpp \
    msgmanagerdialog.cpp \
    msgrecordform.cpp \
    navigationform.cpp \
    showpagesourcedialog.cpp \
    toolbarform.cpp

HEADERS += \
    aboatdialog.h \
    cwebengineview.h \
    lastlogsform.h \
    loadstateform.h \
    logitemform.h \
    maindialog.h \
    msgmanagerdialog.h \
    msgrecordform.h \
    navigationform.h \
    showpagesourcedialog.h \
    toolbarform.h

FORMS += \
    aboatdialog.ui \
    lastlogsform.ui \
    loadstateform.ui \
    logitemform.ui \
    maindialog.ui \
    msgmanagerdialog.ui \
    msgrecordform.ui \
    navigationform.ui \
    showpagesourcedialog.ui \
    toolbarform.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH += E:/software/demo2/libqtcore/libqtcore/includes \
              $$PWD/QScintilla/includes

CONFIG(release,debug|release){
    LIBS += E:/software/demo2/libqtcore/libqtcore/libs/qtCore.lib \
            $$PWD/tcmalloc/libtcmalloc_minimal.lib \
            $$PWD/QScintilla/libs/qscintilla2_qt5.lib
}else{
    LIBS += E:/software/demo2/libqtcore/libqtcore/libs/qtCoreD.lib \
            $$PWD/tcmalloc/libtcmalloc_minimal.lib \
            $$PWD/QScintilla/libs/qscintilla2_qt5.lib
}

RESOURCES += \
    resource.qrc
