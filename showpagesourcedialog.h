﻿#ifndef SHOWPAGESOURCEDIALOG_H
#define SHOWPAGESOURCEDIALOG_H

#include <QDialog>

#include "Qsci/qsciscintilla.h"
#include "Qsci/qscilexercss.h"
#include "Qsci/qsciapis.h"

#include "Qsci/qscilexercpp.h"
#include "Qsci/qscilexerjavascript.h"
#include "Qsci/qscilexerhtml.h"
#include "Qsci/qscilexerlua.h"
#include "Qsci/qscilexercss.h"
#include "Qsci/qscilexersql.h"
#include "Qsci/qscilexerxml.h"
#include "Qsci/qscilexercustom.h"

namespace Ui {
class ShowPageSourceDialog;
}

class ShowPageSourceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ShowPageSourceDialog(QWidget *parent = nullptr);
    ~ShowPageSourceDialog();

    void showcontent(QString data);

private:
    Ui::ShowPageSourceDialog *ui;

    QHash<QString,QsciLexer*> m_sciLexerList;
    QsciScintilla *m_editScintilla;
};

#endif // SHOWPAGESOURCEDIALOG_H
