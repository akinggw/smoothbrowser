﻿#include "aboatdialog.h"
#include "ui_aboatdialog.h"

AboatDialog::AboatDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboatDialog)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::Dialog | Qt::WindowSystemMenuHint | Qt::WindowCloseButtonHint);
}

AboatDialog::~AboatDialog()
{
    delete ui;
}

void AboatDialog::setParam(QString title,QString version)
{
    if(title.isEmpty() || version.isEmpty())
        return;

    ui->label_2->setText(title);
    ui->label_3->setText(version);
}

void AboatDialog::on_pushButton_clicked()
{
    this->done(1);
}
