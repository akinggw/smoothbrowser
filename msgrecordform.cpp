﻿#include "msgrecordform.h"
#include "ui_msgrecordform.h"

#include "maindialog.h"

#include <QCheckBox>

MsgRecordForm::MsgRecordForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MsgRecordForm),
    m_startCheckBox(NULL),
    m_isMultSelected(false),
    m_pageType(-1)
{
    ui->setupUi(this);
}

MsgRecordForm::~MsgRecordForm()
{
    delete ui;
}

void MsgRecordForm::DeleteSelectedRecords(void)
{
    QMap<int,tagRcordItem>::iterator iter = m_RecordList.begin();
    for(;iter != m_RecordList.end();)
    {
        if(!iter.value().checkbox->isChecked())
        {
            ++iter;
            continue;
        }

        if(m_pageType == 0)
            MainDialog::getSingleton().deleteLogByIndex(iter.key());
        else if(m_pageType == 1)
            MainDialog::getSingleton().deleteLableByIndex(iter.key());

        ui->listWidget->removeItemWidget(iter.value().item);

        m_RecordList2.remove(iter.key());

        disconnect(iter.value().checkbox,
                SIGNAL(stateChanged(int)),
                this,
                SLOT(onprocessCheckBoxstateChanged(int)));

        iter.value().checkbox->deleteLater();
        delete iter.value().item;

        iter = m_RecordList.erase(iter);
    }
}

void MsgRecordForm::onprocessCheckBoxstateChanged(int state)
{
    if(ui->listWidget->currentItem() == NULL)
        return;

    QCheckBox *pItemCheck = static_cast<QCheckBox*>(ui->listWidget->itemWidget(ui->listWidget->currentItem()));
    if(pItemCheck == NULL)
        return;

    if(m_startCheckBox != NULL &&
       pItemCheck->isChecked() &&
       m_isMultSelected &&
       m_startCheckBox != pItemCheck)
    {
        bool isStart = false;

        for(int i=0;i<m_RecordList2.size();i++)
        {
            if(m_RecordList2[i].checkbox == m_startCheckBox)
                isStart = true;

            if(m_RecordList2[i].checkbox == pItemCheck)
                break;

            if(isStart)
                m_RecordList2[i].checkbox->setChecked(true);
        }
    }

    m_startCheckBox = pItemCheck;
}

void MsgRecordForm::LoadRecordData(int type,QString keyStr)
{
    m_pageType = type;
    ui->listWidget->clear();
    m_RecordList.clear();
    m_RecordList2.clear();

    if(type == 0)
    {
        QVector<tagLog> pallLogs = MainDialog::getSingleton().getalllogs();
        for(int i=0;i<pallLogs.size();i++)
        {
            if(keyStr != "" && !pallLogs[i].name.contains(keyStr))
                continue;

            QListWidgetItem *pItem = new QListWidgetItem(ui->listWidget);
            QCheckBox *pItemCheck = new QCheckBox(pallLogs[i].name);
            pItemCheck->setToolTip(pallLogs[i].url);

            connect(pItemCheck,
                    SIGNAL(stateChanged(int)),
                    this,
                    SLOT(onprocessCheckBoxstateChanged(int)));

            ui->listWidget->addItem(pItem);
            ui->listWidget->setItemWidget(pItem,pItemCheck);

            m_RecordList[i] = tagRcordItem(pItem,pItemCheck);
            m_RecordList2.push_back(tagRcordItem(pItem,pItemCheck));
        }
    }
    else if(type == 1)
    {
        QVector<tagLabel> palllabels = MainDialog::getSingleton().getalllabels();
        for(int i=0;i<palllabels.size();i++)
        {
            if(keyStr != "" && !palllabels[i].name.contains(keyStr))
                continue;

            QListWidgetItem *pItem = new QListWidgetItem(ui->listWidget);
            QCheckBox *pItemCheck = new QCheckBox(palllabels[i].name);
            pItemCheck->setToolTip(palllabels[i].url);

            connect(pItemCheck,
                    SIGNAL(stateChanged(int)),
                    this,
                    SLOT(onprocessCheckBoxstateChanged(int)));

            ui->listWidget->addItem(pItem);
            ui->listWidget->setItemWidget(pItem,pItemCheck);

            m_RecordList[i] = tagRcordItem(pItem,pItemCheck);
            m_RecordList2.push_back(tagRcordItem(pItem,pItemCheck));
        }
    }
}

void MsgRecordForm::on_listWidget_itemPressed(QListWidgetItem *item)
{

}
