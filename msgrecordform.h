﻿#ifndef MSGRECORDFORM_H
#define MSGRECORDFORM_H

#include <QWidget>
#include <QCheckBox>
#include <QListWidgetItem>
#include <QMap>

namespace Ui {
class MsgRecordForm;
}

struct tagRcordItem
{
    tagRcordItem()
        : item(NULL),checkbox(NULL) {}
    tagRcordItem(QListWidgetItem *i,QCheckBox *c)
        : item(i),checkbox(c) {}

    QListWidgetItem *item;
    QCheckBox *checkbox;
};

class MsgRecordForm : public QWidget
{
    Q_OBJECT

public:
    explicit MsgRecordForm(QWidget *parent = nullptr);
    ~MsgRecordForm();

    void LoadRecordData(int type,QString keyStr="");
    void DeleteSelectedRecords(void);
    inline void setisMultSelected(bool issel) { m_isMultSelected = issel; }

protected slots:
    void onprocessCheckBoxstateChanged(int state);

private slots:
    void on_listWidget_itemPressed(QListWidgetItem *item);

private:
    Ui::MsgRecordForm *ui;
    QMap<int,tagRcordItem> m_RecordList;
    QVector<tagRcordItem> m_RecordList2;
    QCheckBox *m_startCheckBox;
    bool m_isMultSelected;
    int m_pageType;
};

#endif // MSGRECORDFORM_H
