﻿#ifndef TOOLBARFORM_H
#define TOOLBARFORM_H

#include <QWidget>
#include <QUrl>

namespace Ui {
class ToolBarForm;
}

class ToolBarForm : public QWidget
{
    Q_OBJECT

public:
    explicit ToolBarForm(QWidget *parent = nullptr);
    ~ToolBarForm();

    /// 设置当前要加载的网址
    void setCurrentUrl(QUrl url);
    /// 得到当前要加载的网址
    inline QUrl getCurrentUrl(void) { return m_curOperUrl; }
    /// 得到当前输入的网址
    QUrl getCurrentInputUrl(void);
    /// 设置当前网址是否已经保存
    void setLabelChecked(bool checked);

protected:
    bool eventFilter(QObject *obj, QEvent *e);

signals:
    /// 上一页
    void signaltoLastPage(void);
    /// 下一页
    void signaltoNextPage(void);
    /// 重新加载页面
    void signalreloadPage(void);
    /// 打开页面
    void signalOpenUrl(QUrl url);
    /// 保存标签
    void signalSaveLabel(bool isSava);
    /// 系统设置菜单
    void signalSystemSetMenu(void);

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_6_clicked();
    void on_pushButton_5_clicked();
    void on_lineEdit_cursorPositionChanged(int arg1, int arg2);

private:
    Ui::ToolBarForm *ui;
    QUrl m_curOperUrl;
};

#endif // TOOLBARFORM_H
